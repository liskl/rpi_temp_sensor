import logging
import os.path
import configparser

logger = logging.getLogger(__name__)


config_file = '/etc/liskl/defaults.cfg'




def write_default_config_if_non_existant( conf_file=config_file ):
        config = configparser.ConfigParser()
        if os.path.exists(conf_file) == False:
                print("config file did not exist, creating a basic one.")
                config.add_section('adafruitio')
                config.set('adafruitio', 'username', '< adafruitio username >')
                config.set('adafruitio', 'password', '< adafruitio key >')
                # Writing our configuration file to conf_file
                with open(conf_file, 'wb') as configfile:
                        config.write(configfile)

def ConfigSectionMap(section):
        dict1 = {}
        options = Config.options(section)
        for option in options:
                try:
                        dict1[option] = Config.get(section, option)
                        if dict1[option] == -1:
                                DebugPrint("skip: %s" % option)
                except:
                        print("exception on %s!" % option)
                        dict1[option] = None
        return dict1

def read_config( conf_file=config_file ):
        logger.info('Reading Configuration file.')
        settings = configparser.ConfigParser()
        settings._interpolation = configparser.ExtendedInterpolation()
        settings.read(conf_file)

        credentials = {}
        credentials['adafruitIO-user'] = settings.get('adafruitio', 'username')
        credentials['adafruitIO-key'] = settings.get('adafruitio', 'key')

        if 'adafruitio' not in settings.sections():
                print( "ERROR: please add the [adafruitio] section to {}.".format(conf_file) )
                exit(1)
        return settings, credentials
