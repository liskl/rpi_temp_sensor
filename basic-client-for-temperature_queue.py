#!/usr/bin/env python3.5

import logging
import threading
import paho.mqtt.client as mqtt

FORMAT = '%(asctime)-15s %(levelname)s %(funcName)s %(message)s'
#logging.basicConfig(level=logging.INFO, format=FORMAT )
logging.basicConfig(level=logging.DEBUG, format=FORMAT )



def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    topic2sub = ("com.liskl.sensors/7ab7ceef/_tempdata", 0)
    #topic2sub = ("$SYS/#", 0)

    client.subscribe(topic2sub)
    print('subscribing to: {}'.format(topic2sub))

def on_message(client, userdata, message):
	temperature = "{0:0.2f}".format(float(message.payload))
	logging.info("Received message '" + str(temperature) + "' on topic '" + message.topic + "' with QoS " + str(message.qos))


client = mqtt.Client( client_id='store_temperature')

client.on_connect = on_connect
client.on_message = on_message

client.connect("mqtt.liskl.com", 1883, 60)
run = True
while run:
    client.loop()
