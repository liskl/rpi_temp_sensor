#!/bin/bash


/bin/mount -o remount,rw /boot;
/bin/mount -o remount,rw /;

systemctl stop sensors2mqtt.service;
systemctl disable sensors2mqtt.service;

mv "/home/deploy/rpi_temp_sensor/sensors2mqtt.service" "/lib/systemd/system/sensors2mqtt.service";
mv "/home/deploy/rpi_temp_sensor/deploy.sh" "/usr/local/bin/sensor-deploy.sh";

chmod +x "/usr/local/bin/sensor-deploy.sh";
chown deploy:deploy "/usr/local/bin/sensor-deploy.sh";

systemctl enable sensors2mqtt.service;
systemctl restart sensors2mqtt.service;

/bin/mount -o remount,ro /boot;
/bin/mount -o remount,ro /;