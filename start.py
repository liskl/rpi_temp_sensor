#!/usr/bin/env python3

import os, sys
import logging

import glob
import time
import json
import socket
import configparser
from Adafruit_IO import *
import paho.mqtt.client as mqtt
from datetime import datetime as dt
import RPi.GPIO as GPIO

from liskl.util import getserial
from liskl.logging import ConsoleHandler
from liskl.logging import setup_logging as initialize_logging
from liskl.configuration import write_default_config_if_non_existant
from liskl.configuration import ConfigSectionMap
from liskl.configuration import read_config

mqtt_topic_base = 'com.liskl.sensors'
mqtt_sensor_id = '{}'.format( getserial() )
mqtt_topic_hostname = ''

mqtt_log_topic = ''


#def _log_to_mqtt(topic='com.liskl.sensors/{}_log'.format(sensor_serial) , msg):
	
logger = initialize_logging(__name__)

#logger = logging.getLogger(__name__)
#formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

#fh = logging.FileHandler('/var/log/mqtt_logger.log')
#fh.setFormatter(formatter)
#fh.setLevel(logging.DEBUG)
#logger.addHandler(fh)

#ch = ConsoleHandler()
#ch.setLevel(logging.DEBUG)
#logger.addHandler(ch)

server = 'mqtt.liskl.com'
#server = 'io.adafruit.com'

config_file = '/etc/liskl/defaults.cfg'
PATH      = 'liskl' + '/#'

# Pin Definitons:
Relay1Pin = 22
Relay2Pin = 18
Relay3Pin = 16
Relay4Pin = 15
Pir1Pin   = 13

global LastTempMeasurement, LastMotionSenseMeasurement

LastTempMeasurement = None
LastMotionSenseMeasurement = None

#Tell the script when the world started :)
beginning = dt(1970, 1, 1)

# Pin Setup:
GPIO.setmode(GPIO.BOARD)

RELAYS = [ {'name': 'Relay 1', 'pin': Relay1Pin},
           {'name': 'Relay 2', 'pin': Relay2Pin},
           {'name': 'Relay 3', 'pin': Relay3Pin},
           {'name': 'Relay 4', 'pin': Relay4Pin},
         ]

for Relay in RELAYS:
        GPIO.setup(Relay['pin'], GPIO.OUT)

GPIO.setup(Pir1Pin, GPIO.IN)

def connected(client, userdata, flags, rc, sensor_serial=str(getserial()).lstrip('0') ):
	logger.info('Connected to "{}" with result code {}'.format(server, str(rc)))

	# Subscribing in on_connect() means that if we lose the connection and
	# reconnect then subscriptions will be renewed.
	#client.subscribe("$SYS/#")

	topics2Subscribe2 = [	'{}/{}/_control'.format(mqtt_topic_base, sensor_serial),
				'{}/list'.format(mqtt_topic_base),
	]

	for topic in topics2Subscribe2:
		logger.info ('Subscribing to "{}".'.format(topic))
		client.subscribe('{}'.format(topic))

	publish_list = [{ 'topic': '{}/list'.format(mqtt_topic_base), 'message': sensor_serial },
					{ 'topic': '{}/{}/_log'.format(mqtt_topic_base, sensor_serial), 'message': 'Successfully Connected to {}'.format(server) },
					#{ 'topic': '', 'message': ''}
	]

	for item2publish in publish_list:
		for topic, message in item2publish.items():
			client.publish(topic, message )

	client.will_set('{}/{}/_log'.format(mqtt_topic_base, sensor_serial), payload='Lost Connection.', qos=0, retain=True)

def disconnected(client, sensor_serial=str(getserial()).lstrip('0')):
	print('Disconnected from "{}"!'.format(server), flush=True)
	logger.info('Disconnected from server: {}.'.format(server))
	exit(1)


def setRelayToSetting( relay, setting=False):
	name = RELAYS[relay][name]
	pin = RELAYS[relay][pin]

	if setting == True:
		relay_setting = GPIO.HIGH
	else:
		relay_setting = GPIO.LOW

	print('setting {} to {}.'.format(relay_setting))
	GPIO.output(pin, relay_setting)


def message(client, userdata, msg, sensor_serial=str(getserial()).lstrip('0')):
	import subprocess

	if msg.topic == '{}/{}_control'.format(mqtt_topic_base, sensor_serial):
		print(msg.topic+" "+str(msg.payload))
		client.publish('{}/{}/_log'.format(mqtt_topic_base, sensor_serial), 'Received CNC Message: {}'.format( '{} {}'.format(msg.topic, str(msg.payload) )))
		#{ 'command': { 'r1': True, 'r2': True, 'r3': True, 'r4': True, } }
		if str(msg.payload):
			json_payload = json.loads(msg.payload)
			if command in json_payload:
				for key, value in command.items(): 
					if (key == 'r1') or (key == 'r2') or (key == 'r3') or (key == 'r4'):
						client.publish('{}/{}/_log'.format(mqtt_topic_base, sensor_serial), 'MQTT Initiated Relay Control, {} to {}'.format(key, value))
						setRelayToSetting( key, setting=value)
					if (key == 'reboot'):
						logging.debug("running Reboot command")
						client.publish('{}/{}/_log'.format(mqtt_topic_base, sensor_serial), 'MQTT Initiated reboot of sensor in 1 minute, Commencing.')
						command_instance = subprocess.Popen([ 'shutdown', '-r', '+1' ])
	else:
		client.publish('{}/{}/_log'.format(mqtt_topic_base, sensor_serial), 'Received Unknown Message from topic: {}, payload: {}'.format( msg.topic, str(msg.payload) ))
		print(msg.topic+" "+str(msg.payload))


def is_generic_mqtt( server=server ):

	if server is 'io.adafruit.com':
		return False
	else:
		return True

def read_temp_raw():
	base_dir = '/sys/bus/w1/devices/'

	try:
		device_folder = glob.glob(base_dir + '28*')[0]
	except:
		print(' No 1W temperature Sensor Detected.')
		time.sleep(5.0)
		exit()

	device_file = device_folder + '/w1_slave'

	f = open(device_file, 'r')
	lines = f.readlines()
	f.close()
	#print('{}'.format(lines), flush=True)
	return lines

def read_temp():
	import json
	from datetime import datetime as dt

	timestamp1 = (dt.utcnow() - beginning).total_seconds()
	lines = read_temp_raw()
	timestamp2 = (dt.utcnow() - beginning).total_seconds()
	timestamp = ( ( timestamp1 + timestamp2 ) / 2 )

	while lines[0].strip()[-3:] != 'YES':
		time.sleep(0.2)
		lines = read_temp_raw()
		print(lines)

	equals_pos = lines[1].find('t=')
	if equals_pos != -1:
		temp_string = lines[1][equals_pos+2:]
		temp_c = ( float(temp_string) / 1000.0 )
		temp_f = (temp_c * 9.0 / 5.0 + 32.0 )
		
		response = { 'temperature': { "fahrenheit": temp_f, "celsius": temp_c }, "timestamp": str(timestamp) }
	
	return json.dumps(response, sort_keys=True)

def read_pir( beginning=beginning ):
	import json
	from datetime import datetime as dt


	timestamp1 = (dt.utcnow() - beginning).total_seconds()
	sensor_reading = GPIO.input(Pir1Pin)
	timestamp2 = (dt.utcnow() - beginning).total_seconds()
	timestamp = ( ( timestamp1 + timestamp2 ) / 2 )

	response = { 'motionsense': { "sensor_reading": sensor_reading }, "timestamp": str(timestamp) }
	return sensor_reading

def hasTempDataChanged( value, LastTempMeasurement=999.99 ):

	if ( LastTempMeasurement is not 999.99 ):
		if ( value == LastTempMeasurement ):
			return False, LastTempMeasurement
		else:
			return True, value
	else:
		return True, value

def hasMotionDataChanged( value, LastMotionSenseMeasurement=999 ):

	if ( LastMotionSenseMeasurement is not None ):
		if ( value == LastMotionSenseMeasurement ):
			return False, LastMotionSenseMeasurement
		else:
			return True, value
	else:
		return True, value


def setup_1w_modules():
	import os
	os.system('modprobe w1-gpio')
	os.system('modprobe w1-therm')

def setup_MQTTClient(username, key):

	if is_generic_mqtt():
		# Create an MQTT client instance.
		client = mqtt.Client(username, key)
	else:
		# Create an MQTT client instance.
		client = MQTTClient(username, key)	

	# Setup the callback functions defined above.
	client.on_connect    = connected
	client.on_disconnect = disconnected
	client.on_message    = message

	if is_generic_mqtt():
		# Connect to the defined mqtt server
		client.connect(server ,1883, 60)
		client.loop_start()
	else:
		# Connect to the Adafruit IO server.
		client.connect()
		client.loop_background()
	
	return client

def pull_json_temp_via_type(temp_type='fahrenheit'):
	import json


	temp_data=read_temp()
	value = json.loads(temp_data)['temperature'][temp_type]
	logger.debug('temperature in {} is {}.'.format(temp_type, value))
	return value

def send_data( client, id, data, type ):
	if (type == 'pir'): 
		topic = '{}/{}/_motionsense'.format(mqtt_topic_base, id )
	elif (type == 'temp'):
		topic = '{}/{}/_tempdata'.format(mqtt_topic_base, id )
	else:
		logger.info('Doing Nothing.')

	logger.info('pushing Value: {}, to topic "{}"'.format( data, topic ))
	client.publish('{}'.format( topic ), data)
        

def reset_Relay( RELAYS=RELAYS, Exclude=[]):

	for relay in RELAYS:
		if relay['pin'] in Exclude:
			pass
		else:
			print('reseting pin: {}.'.format( relay['name']))
			GPIO.output(relay['pin'], GPIO.LOW)



def temp_in_range(value, min=0.00, max=250.00):
	if (value >= min ) and ( value <= max ):
		return True
	else:
		return False

# Setup the 1w Modules for accessing the ds12b80
setup_1w_modules()

# setup configuration to allow for externalized parameters.
write_default_config_if_non_existant()

settings, credentials = read_config()

sensor_serial = str(getserial()).lstrip('0')

client = setup_MQTTClient(credentials['adafruitIO-user'], credentials['adafruitIO-key'])
print('Publishing a new temperature every 10 seconds (press Ctrl-C to quit)...', flush=True)


while True:
	timestamp = (dt.utcnow() - dt(1970, 1, 1)).total_seconds()
	temp_values = []
	motion_values = []
	
	for temp_value in range(5):
		temp_values.append(pull_json_temp_via_type(temp_type='fahrenheit'))
		time.sleep(0.3)

	motion_value = read_pir()

	temp_value = (sum(temp_values)/float(len(temp_values)))
	send_temp, temp_value = hasTempDataChanged( temp_value, LastTempMeasurement=LastTempMeasurement )
	send_motion, motion_value = hasMotionDataChanged( motion_value, LastMotionSenseMeasurement=LastMotionSenseMeasurement )

	if ( send_temp == True ):
		send_data( client, sensor_serial, temp_value, 'temp' )

	if ( send_motion == True ):
		send_data( client, sensor_serial, motion_value, 'pir' )


	#if old_value == value:
	#	if last_timestamp == (timestamp - 600.0 ):
	#		print('timestamps: last 5m send: {}, current timestamp: {}, timestamp needs to be: {} to send.'.format(last_timestamp, timestamp, (timestamp - 600.0 )), flush=True)
#
#			print('sending {}, cause 5 minute interval passed. '.format(value), flush=True)
#			send_data( client, sensor_serial, value )
#			last_timestamp = (dt.utcnow() - dt(1970, 1, 1)).total_seconds()
#		else:
#			logger.info('not sending, 5  minute interval has not been passed. '.format(value))
#	else:
#		send_data( client, sensor_serial, value, type )
#
#	time.sleep(0.05)
#	old_value = value


	#if temp_in_range(value, max=70.00):
	#	GPIO.output(Relay1Pin, GPIO.HIGH)
	#	exclude = [ Relay1Pin ]
	#	reset_Relay( Exclude=exclude )
	#	print('setting pin {} High.'.format(Relay1Pin))

	#elif temp_in_range(value, min=70.01, max=75.00):
	#	exclude = [ Relay2Pin ]
	#	reset_Relay( Exclude=exclude )
	#	GPIO.output(Relay2Pin, GPIO.HIGH)

	#elif temp_in_range(value, min=75.01, max=80.00):
	#	exclude = [ Relay3Pin ]
	#	reset_Relay( Exclude=exclude )
	#	GPIO.output(Relay3Pin, GPIO.HIGH)

	#elif temp_in_range(value, min=80.01):
	#	exclude = [ Relay4Pin ]
	#	reset_Relay( Exclude=exclude )
	#	GPIO.output(Relay4Pin, GPIO.HIGH)

	#else:
	#	pass 



