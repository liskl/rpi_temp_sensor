#!/bin/bash -x

#Deploy newest Sensor.

LOCAL_GIT_REPO='/opt/repo/rpi_temp_sensor';
ORIG_WD='/boot/rpi_temp_sensor';
GIT_WD='/boot/com.liskl.sensor';

MOUNT='/bin/mount'

GIT_URL='git@gitlab.com:liskl/rpi_temp_sensor.git'
GIT_URL='https://gitlab.com/liskl/rpi_temp_sensor.git'

cd "/boot";
${MOUNT} -o remount,rw /boot;
${MOUNT} -o remount,rw /;

rm -rf "${ORIG_WD}" "${GIT_WD}" "${LOCAL_GIT_REPO}";



if [ ! -e "${LOCAL_GIT_REPO}" ]; then
        cd /boot; git clone "${GIT_URL}" --separate-git-dir="${LOCAL_GIT_REPO}";

        cd /home/deploy/rpi_temp_sensor/;
        /home/deploy/rpi_temp_sensor/install.sh;

fi

#git clone "${GIT_URL}";
${MOUNT} -o remount,ro /boot;
${MOUNT} -o remount,ro /;
